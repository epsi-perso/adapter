/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.adapter;

/**
 *
 * @author TVall
 */
public interface Duck {

    void fly();

    void quack();
    
}
