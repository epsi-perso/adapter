/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.adapter;

public class DuckTestDrive {

    //write the duck interface adapter to use as turkey

    public static void main(String[] args) {
        Duck evilDuck = new EvilDuck();
        Turkey madTurkey = new MadTurkey();
        
        Duck duckMadTurkey = new TurkeyDuckAdapter(madTurkey);
        //TODO
        testDuck(evilDuck);
        testDuck(madTurkey);
    }

    static void testDuck(Duck duck) {
        duck.fly();
        duck.quack();
    }
}
