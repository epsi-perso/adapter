/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.i1epsi.adapter;

/**
 *
 * @author TVall
 */
public class TurkeyDuckAdapter implements Duck{
    
    private Turkey turkey;
    
    public TurkeyDuckAdapter(Turkey turkey) {
        this.turkey = turkey;
    }
    
    @override
    public void fly() {
        turkey.fly();
    }
    
    @override
    public void quack(){
        turkey.gobble();
    }
}
